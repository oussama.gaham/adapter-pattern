public class Adapter implements Standard {

	Adaptee adaptee = new Adaptee();

	@Override
	public void operation(int a, int b) {
		int nb = adaptee.operation2(a, b);
		adaptee.operation3(nb);
	}
}
