public class Adaptee {
	public int operation2(int a, int b) {
		return a * b;
	}

	public void operation3(int nb) {
		System.out.printf("Adaptee: Result %d\n", nb);
	}
}
