public class StandardImplementation implements Standard{
	@Override
	public void operation(int a, int b) {
		System.out.printf("Standard: The result of %d times %d is %d\n", a, b, a*b);
	}
}
