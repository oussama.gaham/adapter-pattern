public class Application {
	public static void main(String[] args) {
		Standard standard = new StandardImplementation();
		standard.operation(7, 9);
		Standard adapter = new Adapter();
		adapter.operation(7, 9);
	}
}
